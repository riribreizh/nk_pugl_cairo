/* If you want you own options for Nuklear
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_IMPLEMENTATION
#include <nuklear.h>
*/

#include <stdio.h>
#include <string.h>
#include "nk.h"
#include "../nk_pugl_cairo.h"

struct app {
    nk_pugl_config_t wincfg;
    struct nk_color bg;
    nk_bool running;
};

static void draw(struct nk_context *ctx, struct nk_rect *bounds, void *data) {
    struct app * app = (struct app*)data;
    struct nk_rect rc = {0, 0, 250, bounds->h};
    static enum theme themed = THEME_BLACK;
    static const char *themes[] = {"Black (default)","White","Red","Blue","Dark"};
    int newtheme;

    if (nk_begin(ctx, "Demo", rc, 0))
    {
        enum {EASY, HARD};
        static int op = EASY;
        static int property = 20;

        nk_layout_row_static(ctx, 30, 80, 1);
        if (nk_button_label(ctx, "button"))
            fprintf(stdout, "button pressed\n");
        nk_layout_row_dynamic(ctx, 30, 2);
        if (nk_option_label(ctx, "easy", op == EASY)) op = EASY;
        if (nk_option_label(ctx, "hard", op == HARD)) op = HARD;
        nk_layout_row_dynamic(ctx, 25, 1);
        nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);

        nk_layout_row_dynamic(ctx, 20, 1);
        nk_label(ctx, "background:", NK_TEXT_LEFT);
        nk_layout_row_dynamic(ctx, 25, 1);
        if (nk_combo_begin_color(ctx, app->bg, nk_vec2(nk_widget_width(ctx),400))) {
            nk_layout_row_dynamic(ctx, 120, 1);
            app->bg = nk_rgb_cf(nk_color_picker(ctx, nk_color_cf(app->bg), NK_RGBA));
            nk_layout_row_dynamic(ctx, 25, 1);
            app->bg.r = nk_propertyf(ctx, "#R:", 0, app->bg.r, 1.0f, 0.01f,0.005f);
            app->bg.g = nk_propertyf(ctx, "#G:", 0, app->bg.g, 1.0f, 0.01f,0.005f);
            app->bg.b = nk_propertyf(ctx, "#B:", 0, app->bg.b, 1.0f, 0.01f,0.005f);
            app->bg.a = nk_propertyf(ctx, "#A:", 0, app->bg.a, 1.0f, 0.01f,0.005f);
            nk_combo_end(ctx);
        }
        nk_layout_row_dynamic(ctx, 20, 1);
        nk_label(ctx, "theme:", NK_TEXT_LEFT);
        nk_layout_row_dynamic(ctx, 25, 1);
        newtheme = nk_combo(ctx, themes, NK_LEN(themes), themed, 25, nk_vec2(200, 200));
    }
    nk_end(ctx);

    overview(ctx, nk_rect(260, 10, 500, 600));

    if (newtheme != themed) {
        themed = newtheme;
        set_style(ctx, newtheme);
    }
}

static void init_app(struct app *app) {
    memset(app, 0, sizeof(struct app));
    app->wincfg = (nk_pugl_config_t){
        .width = 1200,
        .height = 900,
        .min.width = 500,
        .min.height = 300,
        .resizable = nk_true,
        .class = "NkPuglCairo",
        .title = "Nuklear/Pugl/Cairo Example",

        .bg = &app->bg,
        /*.fontname = "../../nuklear/extra_font/DroidSans.ttf",*/
        /*.fontsize = 14.0,*/
        .refresh_cb = draw,
        .data = app,
    };

    app->bg = nk_rgb(0, 30, 30);
    app->running = nk_true;
}

int main() {
    struct app fictive_app = { nk_true };
    nk_bool window_exists = nk_true;
    nk_pugl_window_t *win;

    init_app(&fictive_app);
    win = nk_pugl_init(&fictive_app.wincfg);
    nk_pugl_show(win, nk_true);

    while (fictive_app.running) {
        if (!nk_pugl_process_events(win, -1.0))
            fictive_app.running = nk_false;
    }

    nk_pugl_shutdown(win);
    return 0;
}
