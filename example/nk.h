#ifndef NK_H
#define NK_H

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#include <nuklear/nuklear.h>

enum theme {
    THEME_BLACK,
    THEME_WHITE,
    THEME_RED,
    THEME_BLUE,
    THEME_DARK
};

int overview(struct nk_context *ctx, struct nk_rect bounds);
void set_style(struct nk_context *ctx, enum theme theme);

#endif
