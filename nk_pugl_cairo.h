/* nk_pugl_cairo.h - pugl/cairo for nuklear
 *
 * This file is part of nk_pugl_cairo.
 * Copyright 2021 by Richard Gill <richard@houbathecat.fr>
 * SPDX-License-Identifier: GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 *
 * nk_pugl_cairo is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * nk_pugl_cairo is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with nk_pugl_cairo.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contains many copied parts from:
 * - nk_pugl for pugl integration (Artistic License 2.0)
 *   Copyright (c) 2016-2020 Hanspeter Portner (dev@open-music-kontrollers.ch)
 *   https://gitlab.com/OpenMusicKontrollers/nk_pugl
 * - nuklear_xcb for cairo integration (MIT License)
 *   Copyright 2017 Adriano Grieb
 *   https://github.com/griebd/nuklear_xcb
 * - twidgets for cairo implementation of NK_COMMAND_RECT_MULTI_COLOR (MIT License)
 *   Copyright (c) 2019-2020 Xichen Zhou
 *   https://github.com/taiwins/twidgets
 */


#ifndef _NK_PUGL_CAIRO_H
#define _NK_PUGL_CAIRO_H

/* =========================================================================
 *
 *                                  API
 *
 * ========================================================================= */

#ifdef NK_PUGL_CAIRO_EXPOSE_TYPES
#ifndef PUGL_PUGL_H
#include <pugl/pugl.h>
#endif /* !PUGL_PUGL_H */
#endif /* NK_PUGL_CAIRO_EXPOSE_TYPES */

#ifndef NK_NUKLEAR_H_
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#ifdef NK_PUGL_CAIRO_IMPLEMENTATION
#define NK_IMPLEMENTATION
#endif /* NK_PUGL_CAIRO_IMPLEMENTATION */
#include <nuklear.h>
#else
#ifndef NK_INCLUDE_DEFAULT_ALLOCATOR
NK_STATIC_ASSERT(nk_false && "nk_pugl_cairo.h needs NK_INCLUDE_DEFAULT_ALLOCATOR to be defined for Nuklear");
#endif
#endif /* !NK_NUKLEAR_H_ */

#ifdef __cplusplus
extern C {
#endif

#ifndef NK_PUGL_CAIRO_DEFAULT_FONT_SIZE
#define NK_PUGL_CAIRO_DEFAULT_FONT_SIZE 12.0
#endif
#ifndef NK_PUGL_CAIRO_DEFAULT_BG
#define NK_PUGL_CAIRO_DEFAULT_BG    (struct nk_color){45, 55, 65, 255}
#endif

typedef struct nk_pugl_config nk_pugl_config_t;
typedef struct nk_pugl_window nk_pugl_window_t;
typedef void (*nk_pugl_refresh_cb)(struct nk_context *ctx, struct nk_rect *bounds, void *data);

struct nk_pugl_config {
    nk_uint width;
    nk_uint height;
    struct {
        nk_uint width;
        nk_uint height;
    } min;
    struct {
        nk_uint width;
        nk_uint height;
    } max;
    nk_bool resizable;
    nk_bool fixed_aspect;
    const char* class;
    const char* title;

    nk_ptr parent;
    nk_bool threads;

    struct nk_color *bg;
    const char *fontname;
    double fontsize;

    nk_pugl_refresh_cb refresh_cb;
    void *data;
};

NK_API nk_pugl_window_t *nk_pugl_init(nk_pugl_config_t *cfg);
NK_API void nk_pugl_shutdown(nk_pugl_window_t *win);
NK_API void nk_pugl_show(nk_pugl_window_t *win, nk_bool show);
NK_API void nk_pugl_quit(nk_pugl_window_t *win);
NK_API nk_bool nk_pugl_process_events(nk_pugl_window_t *win, double timeout);
NK_API void nk_pugl_redisplay(nk_pugl_window_t *win);
NK_API void nk_pugl_resize(nk_pugl_window_t *win, int width, int height);

NK_API void nk_pugl_copy_to_clipboard(nk_pugl_window_t *win, const char *selection, nk_size len);
NK_API const char *nk_pugl_paste_from_clipboard(nk_pugl_window_t *win, nk_size *len);

#ifdef __cplusplus
}
#endif

#endif /* !_NK_PUGL_CAIRO_H */

/* =========================================================================
 *
 *                               Internal type
 *
 * ========================================================================= */
#if !defined(NK_PUGL_CAIRO_EXPOSE_TYPES) || defined(NK_PUGL_CAIRO_IMPLEMENTATION)

#ifndef PUGL_PUGL_H
#include <pugl/pugl.h>
#endif /* !PUGL_PUGL_H */
#ifndef CAIRO_H
#include <cairo/cairo.h>
#endif /* !CAIRO_H */

struct nk_pugl_window {
    nk_pugl_config_t *config;
    struct nk_context nkctx;
    struct nk_user_font font;
    cairo_font_face_t *font_face;

    PuglWorld *world;
    PuglView *view;
    PuglNativeView native_view;
    PuglMod mods;

    nk_bool quit;
};

#endif /* !NK_PUGL_CAIRO_EXPOSE_TYPES || NK_PUGL_CAIRO_IMPLEMENTATION */

/* =========================================================================
 *
 *                              Implementation
 *
 * ========================================================================= */
#if defined(NK_PUGL_CAIRO_IMPLEMENTATION) && !defined(_NK_PUGL_CAIRO_IMPLEMENTED)
#define _NK_PUGL_CAIRO_IMPLEMENTED

#include <stdlib.h>
#include <ctype.h>
#ifndef PUGL_CAIRO_H
#include <pugl/cairo.h>
#endif /* !PUGL_CAIRO_H */
#ifndef CAIRO_FT_H
#include <cairo/cairo-ft.h>
#endif /* !CAIRO_FT_H */

#define NK_DEG_TO_RAD(x) ((double) x * NK_PI / 180.0)
#define NK_TO_CAIRO(x) ((double) x / 255.0)

#define KEY_TAB '\t'
#define KEY_NEWLINE '\n'
#define KEY_RETURN '\r'
#define KEY_PLUS '+'
#define KEY_MINUS '-'
#define KEY_C 'c'
#define KEY_V 'v'
#define KEY_X 'x'
#define KEY_Z 'z'

NK_INTERN float _nk_cairo_text_width(nk_handle handle, float height __attribute__ ((__unused__)), const char *text, int len);
NK_INTERN void _nk_cairo_init_context(nk_pugl_window_t *win, cairo_t *cr);
NK_INTERN void _nk_cairo_destroy_context(nk_pugl_window_t *win);
NK_API void _nk_cairo_render(nk_pugl_window_t *win, cairo_t *cr);

NK_INTERN void _nk_pugl_expose(nk_pugl_window_t *win, cairo_t *cr);
NK_INTERN void _nk_pugl_editor_copy(nk_handle userdata, const char *buf, int len);
NK_INTERN void _nk_pugl_editor_paste(nk_handle userdata, struct nk_text_edit *editor);
NK_INTERN void _nk_pugl_key_press(struct nk_context *ctx, enum nk_keys key);
NK_INTERN void _nk_pugl_modifiers(nk_pugl_window_t *win, PuglMod mods);
NK_INTERN bool _nk_pugl_key_down(nk_pugl_window_t *win, const PuglKeyEvent *ev);
NK_INTERN nk_bool nk_pugl_key_up(nk_pugl_window_t *win, const PuglKeyEvent *ev);
NK_INTERN PuglStatus _nk_pugl_handle_events(PuglView* view, const PuglEvent* event);

/* =========================================================================
 * Rendering part (cairo related)
 * ========================================================================= */

NK_INTERN float _nk_cairo_text_width(nk_handle handle, float height __attribute__ ((__unused__)), const char *text, int len) {
    cairo_scaled_font_t *font = handle.ptr;
    cairo_glyph_t *glyphs = NULL;
    int num_glyphs;
    cairo_text_extents_t extents;

    cairo_scaled_font_text_to_glyphs(font, 0, 0, text, len, &glyphs, &num_glyphs, NULL, NULL, NULL);
    cairo_scaled_font_glyph_extents(font, glyphs, num_glyphs, &extents);
    cairo_glyph_free(glyphs);

    return extents.x_advance;
}

NK_INTERN void _nk_cairo_set_font(nk_pugl_window_t *win, cairo_t *cr) {
    cairo_font_extents_t extents;
    cairo_scaled_font_t *default_font;

    if (win->config->fontname != NULL) {
        FT_Library library;
        FT_Face face;
        static const cairo_user_data_key_t key;

        if (win->font_face != NULL) {
            cairo_font_face_destroy(win->font_face);
        }

        FT_Init_FreeType(&library);
        FT_New_Face(library, win->config->fontname, 0, &face);
        win->font_face = cairo_ft_font_face_create_for_ft_face(face, 0);
        cairo_font_face_set_user_data(win->font_face, &key, face, (cairo_destroy_func_t)FT_Done_Face);
        cairo_set_font_face(cr, win->font_face);
    }
    if (win->config->fontsize < 0.01) {
        win->config->fontsize = NK_PUGL_CAIRO_DEFAULT_FONT_SIZE;
    }

    cairo_set_font_size(cr, win->config->fontsize);
    default_font = cairo_get_scaled_font(cr);
    cairo_scaled_font_extents(default_font, &extents);

    win->font.userdata.ptr = default_font;
    win->font.height = extents.height;
    win->font.width = _nk_cairo_text_width;
}

NK_INTERN void _nk_cairo_init_context(nk_pugl_window_t *win, cairo_t *cr) {
    /* first deal with font */
    _nk_cairo_set_font(win, cr);

    /* then create the context */
    nk_init_default(&win->nkctx, &win->font);
    win->nkctx.clip.copy = _nk_pugl_editor_copy;
    win->nkctx.clip.paste = _nk_pugl_editor_paste;
    win->nkctx.clip.userdata.ptr = win;
    nk_input_begin(&win->nkctx);
}

NK_INTERN void _nk_cairo_destroy_context(nk_pugl_window_t *win) {
    nk_input_end(&win->nkctx);
    nk_free(&win->nkctx);
}

NK_API void _nk_cairo_render(nk_pugl_window_t *win, cairo_t *cr) {
    const struct nk_command *cmd = NULL;
    void *cmds = nk_buffer_memory(&win->nkctx.memory);

    nk_foreach(cmd, &win->nkctx) {
        switch (cmd->type) {
        case NK_COMMAND_NOP:
            break;
        case NK_COMMAND_SCISSOR:
            {
                const struct nk_command_scissor *s = (const struct nk_command_scissor *)cmd;
                cairo_reset_clip(cr);
                if (s->x >= 0) {
                    cairo_rectangle(cr, s->x - 1, s->y - 1, s->w + 2, s->h + 2);
                    cairo_clip(cr);
                }
            }
            break;
        case NK_COMMAND_LINE:
            {
                const struct nk_command_line *l = (const struct nk_command_line *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(l->color.r), NK_TO_CAIRO(l->color.g), NK_TO_CAIRO(l->color.b), NK_TO_CAIRO(l->color.a));
                cairo_set_line_width(cr, l->line_thickness);
                cairo_move_to(cr, l->begin.x, l->begin.y);
                cairo_line_to(cr, l->end.x, l->end.y);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_CURVE:
            {
                const struct nk_command_curve *q = (const struct nk_command_curve *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(q->color.r), NK_TO_CAIRO(q->color.g), NK_TO_CAIRO(q->color.b), NK_TO_CAIRO(q->color.a));
                cairo_set_line_width(cr, q->line_thickness);
                cairo_move_to(cr, q->begin.x, q->begin.y);
                cairo_curve_to(cr, q->ctrl[0].x, q->ctrl[0].y, q->ctrl[1].x, q->ctrl[1].y, q->end.x, q->end.y);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_RECT:
            {
                const struct nk_command_rect *r = (const struct nk_command_rect *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(r->color.r), NK_TO_CAIRO(r->color.g), NK_TO_CAIRO(r->color.b), NK_TO_CAIRO(r->color.a));
                cairo_set_line_width(cr, r->line_thickness);
                if (r->rounding == 0) {
                    cairo_rectangle(cr, r->x, r->y, r->w, r->h);
                }
                else {
                    int xl = r->x + r->w - r->rounding;
                    int xr = r->x + r->rounding;
                    int yl = r->y + r->h - r->rounding;
                    int yr = r->y + r->rounding;
                    cairo_new_sub_path(cr);
                    cairo_arc(cr, xl, yr, r->rounding, NK_DEG_TO_RAD(-90), NK_DEG_TO_RAD(0));
                    cairo_arc(cr, xl, yl, r->rounding, NK_DEG_TO_RAD(0), NK_DEG_TO_RAD(90));
                    cairo_arc(cr, xr, yl, r->rounding, NK_DEG_TO_RAD(90), NK_DEG_TO_RAD(180));
                    cairo_arc(cr, xr, yr, r->rounding, NK_DEG_TO_RAD(180), NK_DEG_TO_RAD(270));
                    cairo_close_path(cr);
                }
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_RECT_FILLED:
            {
                const struct nk_command_rect_filled *r = (const struct nk_command_rect_filled *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(r->color.r), NK_TO_CAIRO(r->color.g), NK_TO_CAIRO(r->color.b), NK_TO_CAIRO(r->color.a));
                if (r->rounding == 0) {
                    cairo_rectangle(cr, r->x, r->y, r->w, r->h);
                } else {
                    int xl = r->x + r->w - r->rounding;
                    int xr = r->x + r->rounding;
                    int yl = r->y + r->h - r->rounding;
                    int yr = r->y + r->rounding;
                    cairo_new_sub_path(cr);
                    cairo_arc(cr, xl, yr, r->rounding, NK_DEG_TO_RAD(-90), NK_DEG_TO_RAD(0));
                    cairo_arc(cr, xl, yl, r->rounding, NK_DEG_TO_RAD(0), NK_DEG_TO_RAD(90));
                    cairo_arc(cr, xr, yl, r->rounding, NK_DEG_TO_RAD(90), NK_DEG_TO_RAD(180));
                    cairo_arc(cr, xr, yr, r->rounding, NK_DEG_TO_RAD(180), NK_DEG_TO_RAD(270));
                    cairo_close_path(cr);
                }
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_RECT_MULTI_COLOR:
            {
                /* from https://github.com/taiwins/twidgets/blob/master/src/nk_wl_cairo.c */
                const struct nk_command_rect_multi_color *r = (const struct nk_command_rect_multi_color *)cmd;
                cairo_pattern_t *pat = cairo_pattern_create_mesh();
                if (pat) {
                    cairo_mesh_pattern_begin_patch(pat);
                    cairo_mesh_pattern_move_to(pat, r->x, r->y);
                    cairo_mesh_pattern_line_to(pat, r->x, r->y + r->h);
                    cairo_mesh_pattern_line_to(pat, r->x + r->w, r->y + r->h);
                    cairo_mesh_pattern_line_to(pat, r->x + r->w, r->y);
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 0, NK_TO_CAIRO(r->left.r), NK_TO_CAIRO(r->left.g), NK_TO_CAIRO(r->left.b), NK_TO_CAIRO(r->left.a));
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 1, NK_TO_CAIRO(r->bottom.r), NK_TO_CAIRO(r->bottom.g), NK_TO_CAIRO(r->bottom.b), NK_TO_CAIRO(r->bottom.a));
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 2, NK_TO_CAIRO(r->right.r), NK_TO_CAIRO(r->right.g), NK_TO_CAIRO(r->right.b), NK_TO_CAIRO(r->right.a));
                    cairo_mesh_pattern_set_corner_color_rgba(pat, 3, NK_TO_CAIRO(r->top.r), NK_TO_CAIRO(r->top.g), NK_TO_CAIRO(r->top.b), NK_TO_CAIRO(r->top.a));
                    cairo_mesh_pattern_end_patch(pat);

                    cairo_rectangle(cr, r->x, r->y, r->w, r->h);
                    cairo_set_source(cr, pat);
                    cairo_fill(cr);
                    cairo_pattern_destroy(pat);
                }
            }
            break;
        case NK_COMMAND_CIRCLE:
            {
                const struct nk_command_circle *c = (const struct nk_command_circle *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(c->color.r), NK_TO_CAIRO(c->color.g), NK_TO_CAIRO(c->color.b), NK_TO_CAIRO(c->color.a));
                cairo_set_line_width(cr, c->line_thickness);
                cairo_save(cr);
                cairo_translate(cr, c->x + c->w / 2.0, c->y + c->h / 2.0);
                cairo_scale(cr, c->w / 2.0, c->h / 2.0);
                cairo_arc(cr, 0, 0, 1, NK_DEG_TO_RAD(0), NK_DEG_TO_RAD(360));
                cairo_restore(cr);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_CIRCLE_FILLED:
            {
                const struct nk_command_circle_filled *c = (const struct nk_command_circle_filled *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(c->color.r), NK_TO_CAIRO(c->color.g), NK_TO_CAIRO(c->color.b), NK_TO_CAIRO(c->color.a));
                cairo_save(cr);
                cairo_translate(cr, c->x + c->w / 2.0, c->y + c->h / 2.0);
                cairo_scale(cr, c->w / 2.0, c->h / 2.0);
                cairo_arc(cr, 0, 0, 1, NK_DEG_TO_RAD(0), NK_DEG_TO_RAD(360));
                cairo_restore(cr);
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_ARC:
            {
                const struct nk_command_arc *a = (const struct nk_command_arc*) cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(a->color.r), NK_TO_CAIRO(a->color.g), NK_TO_CAIRO(a->color.b), NK_TO_CAIRO(a->color.a));
                cairo_set_line_width(cr, a->line_thickness);
                cairo_arc(cr, a->cx, a->cy, a->r, NK_DEG_TO_RAD(a->a[0]), NK_DEG_TO_RAD(a->a[1]));
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_ARC_FILLED:
            {
                const struct nk_command_arc_filled *a = (const struct nk_command_arc_filled*)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(a->color.r), NK_TO_CAIRO(a->color.g), NK_TO_CAIRO(a->color.b), NK_TO_CAIRO(a->color.a));
                cairo_arc(cr, a->cx, a->cy, a->r, NK_DEG_TO_RAD(a->a[0]), NK_DEG_TO_RAD(a->a[1]));
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_TRIANGLE:
            {
                const struct nk_command_triangle *t = (const struct nk_command_triangle *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(t->color.r), NK_TO_CAIRO(t->color.g), NK_TO_CAIRO(t->color.b), NK_TO_CAIRO(t->color.a));
                cairo_set_line_width(cr, t->line_thickness);
                cairo_move_to(cr, t->a.x, t->a.y);
                cairo_line_to(cr, t->b.x, t->b.y);
                cairo_line_to(cr, t->c.x, t->c.y);
                cairo_close_path(cr);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_TRIANGLE_FILLED:
            {
                const struct nk_command_triangle_filled *t = (const struct nk_command_triangle_filled *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(t->color.r), NK_TO_CAIRO(t->color.g), NK_TO_CAIRO(t->color.b), NK_TO_CAIRO(t->color.a));
                cairo_move_to(cr, t->a.x, t->a.y);
                cairo_line_to(cr, t->b.x, t->b.y);
                cairo_line_to(cr, t->c.x, t->c.y);
                cairo_close_path(cr);
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_POLYGON:
            {
                int i;
                const struct nk_command_polygon *p = (const struct nk_command_polygon *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(p->color.r), NK_TO_CAIRO(p->color.g), NK_TO_CAIRO(p->color.b), NK_TO_CAIRO(p->color.a));
                cairo_set_line_width(cr, p->line_thickness);
                cairo_move_to(cr, p->points[0].x, p->points[0].y);
                for (i = 1; i < p->point_count; ++i) {
                    cairo_line_to(cr, p->points[i].x, p->points[i].y);
                }
                cairo_close_path(cr);
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_POLYGON_FILLED:
            {
                int i;
                const struct nk_command_polygon_filled *p = (const struct nk_command_polygon_filled *)cmd;
                cairo_set_source_rgba (cr, NK_TO_CAIRO(p->color.r), NK_TO_CAIRO(p->color.g), NK_TO_CAIRO(p->color.b), NK_TO_CAIRO(p->color.a));
                cairo_move_to(cr, p->points[0].x, p->points[0].y);
                for (i = 1; i < p->point_count; ++i) {
                    cairo_line_to(cr, p->points[i].x, p->points[i].y);
                }
                cairo_close_path(cr);
                cairo_fill(cr);
            }
            break;
        case NK_COMMAND_POLYLINE:
            {
                int i;
                const struct nk_command_polyline *p = (const struct nk_command_polyline *)cmd;
                cairo_set_source_rgba(cr, NK_TO_CAIRO(p->color.r), NK_TO_CAIRO(p->color.g), NK_TO_CAIRO(p->color.b), NK_TO_CAIRO(p->color.a));
                cairo_set_line_width(cr, p->line_thickness);
                cairo_move_to(cr, p->points[0].x, p->points[0].y);
                for (i = 1; i < p->point_count; ++i) {
                    cairo_line_to(cr, p->points[i].x, p->points[i].y);
                }
                cairo_stroke(cr);
            }
            break;
        case NK_COMMAND_TEXT:
            {
                const struct nk_command_text *t = (const struct nk_command_text *)cmd;
                cairo_glyph_t *glyphs = NULL;
                int num_glyphs;
                cairo_text_cluster_t *clusters = NULL;
                int num_clusters;
                cairo_text_cluster_flags_t cluster_flags;
                cairo_font_extents_t extents;

                cairo_set_source_rgba(cr, NK_TO_CAIRO(t->foreground.r), NK_TO_CAIRO(t->foreground.g), NK_TO_CAIRO(t->foreground.b), NK_TO_CAIRO(t->foreground.a));
                cairo_scaled_font_extents(t->font->userdata.ptr, &extents);
                cairo_scaled_font_text_to_glyphs(t->font->userdata.ptr,
                        t->x, t->y + extents.ascent, t->string, t->length,
                        &glyphs, &num_glyphs, &clusters, &num_clusters,
                        &cluster_flags);
                cairo_show_text_glyphs(cr, t->string, t->length, glyphs,
                        num_glyphs, clusters, num_clusters,
                        cluster_flags);
                cairo_glyph_free(glyphs);
                cairo_text_cluster_free(clusters);
            }
            break;
        case NK_COMMAND_IMAGE:
            {
                /* from https://github.com/taiwins/twidgets/blob/master/src/nk_wl_cairo.c */
                const struct nk_command_image *im = (const struct nk_command_image *)cmd;
                cairo_surface_t *img_surf;
                double sw = (double)im->w / (double)im->img.region[2];
                double sh = (double)im->h / (double)im->img.region[3];
                cairo_format_t format = CAIRO_FORMAT_ARGB32;
                int stride = cairo_format_stride_for_width(format, im->img.w);

                if (!im->img.handle.ptr) break;
                img_surf = cairo_image_surface_create_for_data(im->img.handle.ptr, format, im->img.w, im->img.h, stride);
                if (!img_surf) break;
                cairo_save(cr);

                cairo_rectangle(cr, im->x, im->y, im->w, im->h);
                /* scale here, if after source set, the scale would not apply to source
                 * surface
                 */
                cairo_scale(cr, sw, sh);
                /* the coordinates system in cairo is not intuitive, scale, translate,
                 * are applied to source. Refer to
                 * "https://www.cairographics.org/FAQ/#paint_from_a_surface" for details
                 *
                 * if you set source_origin to (0,0), it would be like source origin
                 * aligned to dest origin, then if you draw a rectangle on (x, y, w, h).
                 * it would clip out the (x, y, w, h) of the source on you dest as well.
                 */
                cairo_set_source_surface(cr, img_surf, im->x/sw - im->img.region[0], im->y/sh - im->img.region[1]);
                cairo_fill(cr);

                cairo_restore(cr);
                cairo_surface_destroy(img_surf);
            }
            break;
        case NK_COMMAND_CUSTOM:
            {
	            const struct nk_command_custom *cu = (const struct nk_command_custom *)cmd;
                if (cu->callback) {
                    cu->callback(cr, cu->x, cu->y, cu->w, cu->h, cu->callback_data);
                }
            }
            break;
        default:
            break;
        }
    }
}

/* =========================================================================
 * Platform part (pugl related)
 * ========================================================================= */

NK_INTERN void _nk_pugl_expose(nk_pugl_window_t *win, cairo_t *cr) {
    struct nk_context *ctx = &win->nkctx;
    struct nk_color *bg = win->config->bg;
    struct nk_rect winbounds = nk_rect(0, 0, win->config->width, win->config->height);

    /* call user function */
    if (win->config->refresh_cb) win->config->refresh_cb(ctx, &winbounds, win->config->data);

    /* draw background */
    cairo_rectangle(cr, 0, 0, win->config->width, win->config->height);
    cairo_set_source_rgb(cr, NK_TO_CAIRO(bg->r), NK_TO_CAIRO(bg->g), NK_TO_CAIRO(bg->b));
    cairo_fill(cr);

    /* restore font */
    cairo_set_font_face(cr, win->font_face);
    cairo_set_font_size(cr, win->config->fontsize);

    /* render nk commands */
    _nk_cairo_render(win, cr);
    nk_clear(&win->nkctx);
}

NK_INTERN void _nk_pugl_editor_copy(nk_handle userdata, const char *buf, int len) {
	nk_pugl_copy_to_clipboard((nk_pugl_window_t *)userdata.ptr, buf, len);
}

NK_INTERN void _nk_pugl_editor_paste(nk_handle userdata, struct nk_text_edit *editor) {
	nk_pugl_window_t *win = userdata.ptr;

	size_t len;
	const char *selection = nk_pugl_paste_from_clipboard((nk_pugl_window_t *)userdata.ptr, &len);
	if (selection) nk_textedit_paste(editor, selection, len);
}

NK_INTERN void _nk_pugl_key_press(struct nk_context *ctx, enum nk_keys key) {
	nk_input_key(ctx, key, nk_true);
	nk_input_key(ctx, key, nk_false);
}

NK_INTERN void _nk_pugl_modifiers(nk_pugl_window_t *win, PuglMod mods) {
	struct nk_context *ctx = &win->nkctx;
	if (win->mods != mods) {
        /* modifiers changed */
		if ((win->mods & PUGL_MOD_SHIFT) != (mods & PUGL_MOD_SHIFT)) {
			nk_input_key(ctx, NK_KEY_SHIFT, (mods & PUGL_MOD_SHIFT) == PUGL_MOD_SHIFT);
		}
		if ((win->mods & PUGL_MOD_CTRL) != (mods & PUGL_MOD_CTRL)) {
			nk_input_key(ctx, NK_KEY_CTRL, (mods & PUGL_MOD_CTRL) == PUGL_MOD_CTRL);
		}
		if ((win->mods & PUGL_MOD_ALT) != (mods & PUGL_MOD_ALT)) {
			/* not yet supported in nuklear */
		}
		if ((win->mods & PUGL_MOD_SUPER) != (mods & PUGL_MOD_SUPER)) {
			/* not yet supported in nuklear */
		}
		win->mods = mods;
	}
}

NK_INTERN bool _nk_pugl_key_down(nk_pugl_window_t *win, const PuglKeyEvent *ev) {
	struct nk_context *ctx = &win->nkctx;
#if defined(__APPLE__)
	const bool control = ev->state & PUGL_MOD_SUPER;
#else
	const bool control = ev->state & PUGL_MOD_CTRL;
#endif
	const bool shift = ev->state & PUGL_MOD_SHIFT;

	switch(ev->key) {
    case PUGL_KEY_LEFT:
        _nk_pugl_key_press(ctx, control ? NK_KEY_TEXT_WORD_LEFT : NK_KEY_LEFT);
        break;
    case PUGL_KEY_RIGHT:
        _nk_pugl_key_press(ctx, control ? NK_KEY_TEXT_WORD_RIGHT : NK_KEY_RIGHT);
        break;
    case PUGL_KEY_UP:
        _nk_pugl_key_press(ctx, NK_KEY_UP);
        break;
    case PUGL_KEY_DOWN:
        _nk_pugl_key_press(ctx, NK_KEY_DOWN);
        break;
    case PUGL_KEY_PAGE_UP:
        _nk_pugl_key_press(ctx, NK_KEY_SCROLL_UP);
        break;
    case PUGL_KEY_PAGE_DOWN:
        _nk_pugl_key_press(ctx, NK_KEY_SCROLL_DOWN);
        break;
    case PUGL_KEY_HOME:
        if(control) {
            _nk_pugl_key_press(ctx, NK_KEY_TEXT_START);
            _nk_pugl_key_press(ctx, NK_KEY_SCROLL_START);
        }
        else {
            _nk_pugl_key_press(ctx, NK_KEY_TEXT_LINE_START);
        }
        break;
    case PUGL_KEY_END:
        if(control) {
            _nk_pugl_key_press(ctx, NK_KEY_TEXT_END);
            _nk_pugl_key_press(ctx, NK_KEY_SCROLL_END);
        }
        else {
            _nk_pugl_key_press(ctx, NK_KEY_TEXT_LINE_END);
        }
        break;
    case PUGL_KEY_INSERT:
        _nk_pugl_key_press(ctx, NK_KEY_TEXT_INSERT_MODE);
        break;
    case PUGL_KEY_SHIFT:
        win->mods |= PUGL_MOD_SHIFT;
        nk_input_key(ctx, NK_KEY_SHIFT, nk_true);
        return true;
    case PUGL_KEY_CTRL:
        win->mods |= PUGL_MOD_CTRL;
        nk_input_key(ctx, NK_KEY_CTRL, nk_true);
        return true;
    case KEY_NEWLINE:   /* fall-through */
    case KEY_RETURN:
        _nk_pugl_key_press(ctx, NK_KEY_ENTER);
        break;
    case KEY_TAB:
        _nk_pugl_key_press(ctx, NK_KEY_TAB);
        break;
    case PUGL_KEY_DELETE:
#if defined(__APPLE__) /* quirk around Apple's Delete key strangeness */
        _nk_pugl_key_press(ctx, NK_KEY_BACKSPACE);
#else
        _nk_pugl_key_press(ctx, NK_KEY_DEL);
#endif
        break;
    case PUGL_KEY_BACKSPACE:
#if defined(__APPLE__) /* quirk around Apple's Delete key strangeness */
        _nk_pugl_key_press(ctx, NK_KEY_DEL);
#else
        _nk_pugl_key_press(ctx, NK_KEY_BACKSPACE);
#endif
        break;
    case PUGL_KEY_ESCAPE:
        _nk_pugl_key_press(ctx, NK_KEY_TEXT_RESET_MODE);
        break;
    default:
        if(control) {
            switch(ev->key) {
            case KEY_C:
                _nk_pugl_key_press(ctx, NK_KEY_COPY);
                break;
            case KEY_V:
                _nk_pugl_key_press(ctx, NK_KEY_PASTE);
                break;
            case KEY_X:
                _nk_pugl_key_press(ctx, NK_KEY_CUT);
                break;
            case KEY_Z:
                _nk_pugl_key_press(ctx, shift ? NK_KEY_TEXT_REDO : NK_KEY_TEXT_UNDO);
                break;
            }
        }
	}

	return false;
}

NK_INTERN nk_bool _nk_pugl_key_up(nk_pugl_window_t *win, const PuglKeyEvent *ev) {
	switch (ev->key) {
    case PUGL_KEY_SHIFT:
        nk_input_key(&win->nkctx, NK_KEY_SHIFT, nk_false);
        win->mods &= ~PUGL_MOD_SHIFT;
    	return nk_true;
    case PUGL_KEY_CTRL:
        nk_input_key(&win->nkctx, NK_KEY_CTRL, nk_false);
        win->mods &= ~PUGL_MOD_CTRL;
    	return nk_true;
	}
	return nk_false;
}

NK_INTERN void _nk_pugl_button(nk_pugl_window_t *win, const PuglButtonEvent *ev, nk_bool pressed) {
    static int mapbtns[] = {
        NK_BUTTON_LEFT,
        NK_BUTTON_RIGHT,
        NK_BUTTON_MIDDLE
    };
    nk_input_button(&win->nkctx, mapbtns[ev->button], ev->x, ev->y, pressed);
}


NK_INTERN PuglStatus _nk_pugl_handle_events(PuglView* view, const PuglEvent* event) {
    nk_pugl_window_t *win = puglGetHandle(view);
    NK_ASSERT(win->view == view);

    switch (event->type) {
    case PUGL_DESTROY:
        /* destroy nuklear context */
        _nk_cairo_destroy_context(win);
        break;
    case PUGL_CONFIGURE:
        if (win->config->width != event->configure.width || win->config->height != event->configure.height) {
            win->config->width = event->configure.width;
            win->config->height = event->configure.height;
            puglPostRedisplay(win->view);
        }
        break;
    case PUGL_EXPOSE:
        {
            cairo_t *cr = (cairo_t *)puglGetContext(view);
            if (win->font.userdata.ptr == NULL) {
                /* our trigger to setup the context.
                * using PUGL_CREATE would be more natural, but the cairo context does not exist yet on this event
                */
                _nk_cairo_init_context(win, cr);
            }
            nk_input_end(&win->nkctx);
            _nk_pugl_expose(win, cr);
            nk_input_begin(&win->nkctx);
        }
        break;
    case PUGL_CLOSE:
        nk_pugl_quit(win);
        break;
    case PUGL_KEY_PRESS:
        if (!_nk_pugl_key_down(win, (const PuglKeyEvent *)event)) {
            _nk_pugl_modifiers(win, event->key.state);
        }
        puglPostRedisplay(win->view);
        break;
    case PUGL_KEY_RELEASE:
        if (!_nk_pugl_key_up(win, (const PuglKeyEvent *)event)) {
            _nk_pugl_modifiers(win, event->key.state);
        }
        puglPostRedisplay(win->view);
        break;
    case PUGL_POINTER_IN:
    case PUGL_POINTER_OUT:
        _nk_pugl_modifiers(win, event->crossing.state);
        puglPostRedisplay(win->view);
        break;
    case PUGL_BUTTON_PRESS:
        _nk_pugl_modifiers(win, event->button.state);
        _nk_pugl_button(win, &event->button, nk_true);
        //nk_input_button(&win->nkctx, event->button.button - 1, event->button.x, event->button.y, nk_true);
        puglPostRedisplay(win->view);
        break;
    case PUGL_BUTTON_RELEASE:
        _nk_pugl_modifiers(win, event->button.state);
        _nk_pugl_button(win, &event->button, nk_false);
        //nk_input_button(&win->nkctx, event->button.button - 1, event->button.x, event->button.y, nk_false);
        puglPostRedisplay(win->view);
        break;
    case PUGL_MOTION:
        _nk_pugl_modifiers(win, event->motion.state);
        nk_input_motion(&win->nkctx, event->motion.x, event->motion.y);
        puglPostRedisplay(win->view);
        break;
    case PUGL_SCROLL:
        _nk_pugl_modifiers(win, event->scroll.state);
        nk_input_scroll(&win->nkctx, nk_vec2(0.0f, event->scroll.dy));
        puglPostRedisplay(win->view);
        break;
    case PUGL_TEXT:
        {
            nk_bool ctrl = event->text.state & PUGL_MOD_CTRL;
            int ch = ctrl ? event->text.character | 0x60 : event->text.character;
            if (isprint(ch)) {
                _nk_pugl_key_press(&win->nkctx, NK_KEY_TEXT_INSERT_MODE);
                nk_input_unicode(&win->nkctx, ch);
            }
        }
        break;
    default:
        break;
    };
    return PUGL_SUCCESS;
}

NK_API nk_pugl_window_t *nk_pugl_init(nk_pugl_config_t *cfg) {
    static struct nk_color def_bg = NK_PUGL_CAIRO_DEFAULT_BG;

    NK_ASSERT(cfg);
    if (cfg->bg == NULL) cfg->bg = &def_bg;

    nk_pugl_window_t *win = calloc(1, sizeof(nk_pugl_window_t));
    NK_ASSERT(win);
    win->config = cfg;

    /* create window (world+view) */
    win->world = puglNewWorld(cfg->parent ? PUGL_MODULE : PUGL_PROGRAM, cfg->threads ? PUGL_WORLD_THREADS : 0);
    if (cfg->class) puglSetClassName(win->world, cfg->class);

    win->view = puglNewView(win->world);
    if (cfg->parent) puglSetParentWindow(win->view, cfg->parent);
    puglSetWindowTitle(win->view, cfg->title ? cfg->title : "Nuklear");

    puglSetFrame(win->view, (PuglRect){
        .x = 0,
        .y = 0,
        .width = cfg->width,
        .height = cfg->height,
    });
    if (cfg->min.width || cfg->min.height) puglSetSizeHint(win->view, PUGL_MIN_SIZE, cfg->min.width, cfg->min.height);
    if (cfg->max.width || cfg->max.height) puglSetSizeHint(win->view, PUGL_MAX_SIZE, cfg->max.width, cfg->max.height);
    if (cfg->fixed_aspect) puglSetSizeHint(win->view, PUGL_FIXED_ASPECT, cfg->width, cfg->height);

    puglSetViewHint(win->view, PUGL_RESIZABLE, cfg->resizable);
    puglSetHandle(win->view, win);
    puglSetEventFunc(win->view, _nk_pugl_handle_events);
    puglSetBackend(win->view, puglCairoBackend());

    PuglStatus st = puglRealize(win->view);
    NK_ASSERT(st == PUGL_SUCCESS);

    win->native_view = puglGetNativeView(win->view);

    return win;
}

NK_API void nk_pugl_shutdown(nk_pugl_window_t *win) {
    NK_ASSERT(win);
    if (win->view) {
        puglFreeView(win->view);
        win->view = NULL;
    }
    if (win->world) {
        puglFreeWorld(win->world);
        win->world = NULL;
    }
    free(win);
}

NK_API void nk_pugl_show(nk_pugl_window_t *win, nk_bool show) {
    NK_ASSERT(win && win->view);
    if (show) puglShow(win->view);
    else puglHide(win->view);
}

NK_API void nk_pugl_quit(nk_pugl_window_t *win) {
    NK_ASSERT(win);
    win->quit = nk_true;
}

NK_API nk_bool nk_pugl_process_events(nk_pugl_window_t *win, double timeout) {
    if (!win->view) {
        return nk_false;
    }
    PuglStatus st = puglUpdate(win->world, timeout);
    return st == PUGL_SUCCESS && !win->quit;
}

NK_API void nk_pugl_redisplay(nk_pugl_window_t *win) {
    if (win->view) puglPostRedisplay(win->view);
}

NK_API void nk_pugl_resize(nk_pugl_window_t *win, int width, int height) {
    if (win->view) {
        win->config->width = width;
        win->config->height = height;
        puglPostRedisplay(win->view);
    }
}

NK_API void nk_pugl_copy_to_clipboard(nk_pugl_window_t *win, const char *selection, size_t len) {
	const char *type = "text/plain";
	puglSetClipboard(win->view, type, selection, len);
}

NK_API const char *nk_pugl_paste_from_clipboard(nk_pugl_window_t *win, size_t *len) {
	return puglGetClipboard(win->view, 0, len);
}

#endif /* NK_PUGL_CAIRO_IMPLEMENTATION && !_NK_PUGL_CAIRO_IMPLEMENTED */
