[Nuklear](https://github.com/Immediate-Mode-UI/Nuklear) immediate-mode GUI using [pugl](https://github.com/lv2/pugl) configured with [Cairo](https://www.cairographics.org/) as backend.

It is of course a single header-only library ;-)

It is based on the excellent work of:
* Hanspeter Portner with [nk_pugl](https://gitlab.com/OpenMusicKontrollers/nk_pugl) for the pugl platform backend (using OpenGL)
* Adriano Grieb with [nuklear_xcb](https://github.com/griebd/nuklear_xcb) for the Cairo rendering backend

![example screenshot](example/screenshot.png)

One important thing for me was the low memory consumtion and CPU usage, hence the use of a blocking on events program loop and the use of Cairo+Freetype for rendering (almost all the memory resides in already loaded shared library on Linux).

> **WARNING: work in progress**

## Building yourself

By default the project is delivered with CMake, but it's possible to integrate it to an existing project providing pugl and cairo.

Then you only need the `nk_pugl_cairo.h` header.

*nk_pugl_cairo* only expect a few things:
  * it includes `<pugl/pugl.h>` if that one was not already included (tests the presence of `PUGL_PUGL_H` define)
  * it includes `<pugl/cairo.h>` if that one was not already included (tests the presence of `PUGL_CAIRO_H` define)
  * the pugl *Cairo* backend must be compiled
  * it includes `<cairo/cairo.h>` (link to cairo) if that one was not already included (tests the presence of `CAIRO_H` define)
  * it includes `<cairo/cairo-ft.h>` (link to freetype) if that one was not already included (tests the presence of `CAIRO_FT_H` define)
  * it includes `<nuklear.h>` if not already included (tests the presence of `NK_NUKLEAR_H_` define)
  * it needs `NK_INCLUDE_DEFAULT_ALLOCATOR` if you include `<nuklear.h>` yourself (it will fail on static assert otherwise)

Unless `NK_PUGL_CAIRO_EXPOSE_TYPES` is defined (see below), all includes except `nuklear.h` are done only in the implementation.

## Using with CMake

That means `nk_pugl_cairo.h` will use its own source tree of *pugl* and *nuklear*.

First init git submodules after the clone
```
git submodule update --init *
```

Then do the usual with CMake:
```
mkdir build
cd build
cmake -S .. -B .
cmake --build .
```

By default the examples target will be built unless this is not the top level directory (part of a `add_subdirectory`).
This can be forced on with the variable `BUILD_EXAMPLES`.
```
cmake -S .. -B . -DBUILD_EXAMPLES=1
```

In any case, the target `nk_pugl_cairo` will be defined with the correct include directory added:
```c
#define NK_PUGL_CAIRO_IMPLEMENTATION
#include <nk_pugl_cairo.h>
```

## Using

The provided example is pretty straightforward and should be enough to understand how to use this header.

The most important piece is the `nk_pugl_config_t` structure used to initialize the backend. It is made of the following fields to create the window and set appropriate callbacks:
* `width`: the initial requested width of the window
* `height`: the initial requested height of the window
* `min`: contains `width` and `height` to define the minimum size of the window
* `max`: contains `width` and `height` to define the maximum size of the window
* `resizable`: the window is resizable or not
* `fixed_aspect`: the window has a fixed aspect ratio (ratio determined by the initial `width` and `height` values)
* `class`: window class name
* `title`: window title string
* `parent`: the identifier of a native parent window in which to embed
* `threads`: initialize threading in pugl (X11 only)
* `bg`: a pointer to a *nk_color* struct to set the window background (opacity not supported)
* `fontname`: use the Freetype default font if `NULL`, a path to a supported font file (aka *ttf*) otherwise
* `fontsize`: Freetype font size
* `refresh_cb`: a pointer to the function that will be called to draw the GUI, see **Drawing function** below
* `data`: a user provided pointer that will be available in the draw function

There are also some defaults that you can override by defining the macros:
* `NK_PUGL_CAIRO_DEFAULT_FONT_SIZE` defaults to `12.0`, when `.fontsize` is zero
* `NK_PUGL_CAIRO_DEFAULT_BG` defaults to a `struct nk_color{45,55,65,255}` if `.bg` is null

Then you deal with a `nk_pugl_window_t` created with `nk_pugl_init()`, that you show with `nk_pugl_show()`.

Finally your main loop is as simple as calling `nk_pugl_process_events()`, which returns true until the window died.

```c
#define NK_PUGL_CAIRO_IMPLEMENTATION
#include <nk_pugl_cairo.h>

int main() {
    nk_pugl_config_t config;
    /* fill the config struct with appropriate values */
    nk_bool running = nk_true;
    nk_pugl_window_t *win = nk_pugl_init(&config);
    nk_pugl_show(win);
    while (running) {
        /* -1.0 means block until an event occurs, it's a value in seconds otherwise */
        running = nk_pugl_process_events(win, -1.0);
    }
    nk_pugl_shutdown(win);
}
```

If you need to access the internal `nk_pugl_window_t` struct, define `NK_PUGL_CAIRO_EXPOSE_TYPES` before including the header.

## Drawing function

The drawing of your UI is done with the refresh callback defined in the `nk_pugl_config_t` struct.

```c
void draw_my_ui(struct nk_context *ctx, struct nk_rect *bounds, void *data) {
    /* do your Nuklear UI stuff with its context.
     * bounds is the rectangle defined by the size of the window
     * data is the data pointer you provided in the config struct
     */
}
```

## License

nk_pugl_cairo is copyleft 2021 Richard Gill, under the GNU GPL 3.0 or above license. See [Copying](./COPYING).

It makes use of valuable parts of different projects:
* [nk_pugl](https://gitlab.com/OpenMusicKontrollers/nk_pugl) for pugl integration (Artistic License 2.0)
  Copyright (c) 2016-2020 Hanspeter Portner (dev@open-music-kontrollers.ch)
* [nuklear_xcb](https://github.com/griebd/nuklear_xcb) for cairo integration (MIT License)
  Copyright 2017 Adriano Grieb
* [twidgets](https://github.com/taiwins/twidgets) for cairo implementation of NK_COMMAND_RECT_MULTI_COLOR (MIT License)
  Copyright (c) 2019-2020 Xichen Zhou

Made for use with:
* [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear)
  Under Public Domain or MIT License
* [Pugl](https://gitlab.com/lv2/pugl)
  Under ISC License
* [Cairo](https://www.cairographics.org/)
  Under GNU Lesser General Public License (LGPL) version 2.1 or the Mozilla Public License (MPL) version 1.1
