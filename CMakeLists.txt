cmake_minimum_required(VERSION 3.15)
project(nk_pugl_cairo)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

if(NOT NK_PUGL_CAIRO_NO_BUILD)
    # -------------------------------------------------------------------------
    # build options

    # enable building examples by default if not a subdirectory of another project
    if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
        set(BUILD_EXAMPLES_DEFAULT ON)
    else()
        set(BUILD_EXAMPLES_DEFAULT OFF)
    endif()

    option(BUILD_EXAMPLES "Build examples" ${BUILD_EXAMPLES_DEFAULT})

    # -------------------------------------------------------------------------
    # sanity checks and various settings

    if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
        message(FATAL_ERROR "Build outside of sources directory.")
    endif()
    if(UNIX AND NOT APPLE)
        set(LINUX 1)
    endif()
    set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)

    add_library(nk_pugl_cairo INTERFACE nk_pugl_cairo.h)
    target_include_directories(nk_pugl_cairo INTERFACE "${CMAKE_CURRENT_LIST_DIR}")

    add_subdirectory(example)

endif() # !NK_PUGL_CAIRO_NO_BUILD
